<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
class Vote_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}

	function get()
	{
		$this->db->select('*');
		$this->db->from('student');
		$this->db->join('vote', 'vote.num = student.num');

		$this->db->order_by('student.major', 'asc');
		$this->db->order_by('student.num', 'asc');

		$query = $this->db->get();
		$result = $query->result();
		
		return $result;
	}

	function count($stuNum)
	{
		$this->db->where('num', $stuNum);
		$count = $this->db->count_all_results('vote');

		return $count;
	}

	function add($vote)
	{
		$this->db->set('created', 'NOW()', false);
		$this->db->insert('vote', array(
			'num' => $vote['stuNum'],
			'council' => $vote['council'],
			'college1' => $vote['college1'],
			'college2' => $vote['college2'],
			'college3' => $vote['college3'],
			'college4' => $vote['college4'],
			'club' => $vote['club']
		));
	}

	function delete($stuNum)
	{
		$this->db->delete('vote', array('num' => $stuNum)); 
	}

	function getNumOfCouncil($id) // 총학생회-투표소
	{
		$this->db->where('council', $id);
		$count = $this->db->count_all_results('vote');

		return $count;
	}

	function getNumOfCollege($type, $id) // 단과대-투표소
	{
		$this->db->where($type, $id);
		$count = $this->db->count_all_results('vote');

		return $count;
	}

	function getNumOfCulb($id) // 총동아리연합회-투표소
	{
		$this->db->where('club', $id);
		$count = $this->db->count_all_results('vote');

		return $count;
	}
}