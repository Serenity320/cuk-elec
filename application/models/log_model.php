<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
class Log_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}

	function addLogPage($log)
	{
		$this->db->set('created', 'NOW()', false);
		$this->db->insert('logpage', array(
			'type' => $log['type'],
			'value' => $log['value'],
			'message' => $log['message'],
			'keyword' => $log['keyword'],
			'id' => $log['id'],
			'ip' => $log['ip']
		));
	}

	function addLogUser($log)
	{
		$this->db->set('created', 'NOW()', false);
		$this->db->insert('loguser', array(
			'type' => $log['type'],
			'value' => $log['value'],
			'message' => $log['message'],
			'keyword' => $log['keyword'],
			'id' => $log['id'],
			'ip' => $log['ip']
		));
	}

	function addLogVote($log)
	{
		$this->db->set('created', 'NOW()', false);
		$this->db->insert('logvote', array(
			'type' => $log['type'],
			'value' => $log['value'],
			'message' => $log['message'],
			'keyword' => $log['keyword'],
			'id' => $log['id'],
			'ip' => $log['ip']
		));
	}
}