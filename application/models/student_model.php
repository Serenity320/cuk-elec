<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
class Student_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}

	function get($stuNum)
	{
		$query = $this->db->get_where('student', array('num' => $stuNum));
		$row = $query->row();
		
		return $row;
	}

	function gets()
	{
		$query = $this->db->get('student');
		$result = $query->result();
		
		return $result;
	}

	function add($student)
	{
		$this->db->insert('student', array(
			'num' => $student['stuNum'],
			'name' => $student['stuName'],
			'major' => $student['stuMajor'],
			'major2' => $student['stuMajor2'],
			'club' => $student['stuClub'],
		));
	}

	function modifyClub($stuNum, $club)
	{
		$this->db->where('num', $stuNum);
		$this->db->update('student', array('club' => $club));
	}

	function getCollage($stuNum)
	{
		$query = $this->db->get_where('student', array('num' => $stuNum));
		$row = $query->row();

		$query = $this->db->get_where('major', array('major' => $row->major));
		$row = $query->row();
		
		return $row;
	}

	function getMajors()
	{
		$query = $this->db->get('major');
		$result = $query->result();
		
		return $result;
	}

	function getTotalOfStudnet() // 총학생회-전체
	{
		$count = $this->db->count_all('student');

		return $count;
	}

	function getTotalOfCollege($type) // 단과대-전체
	{
		$this->db->join('major', 'major.major = student.major');
		$this->db->where('major.type', $type);
		$count = $this->db->count_all_results('student');

		return $count;
	}

	function getTotalOfClub() // 총동아리연합회-전체
	{
		$this->db->where('club', TRUE);
		$count = $this->db->count_all_results('student');

		return $count;
	}
}