<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
class Election_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}

	function get()
	{
		$query = $this->db->get('election', 1);
		$row = $query->row();
		
		return $row;
	}

	function modify($data)
	{
		$this->db->where('no', '1');
		$this->db->set('created', 'NOW()', false);
		$this->db->update('election', $data);

		$this->db->empty_table('vote'); // 투표 정보 초기화
	}
}