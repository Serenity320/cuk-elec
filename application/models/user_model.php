<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
class User_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}

	function get($id)
	{
		$query = $this->db->get_where('user', array('id' => $id));
		$row = $query->row();
		
		return $row;
	}

	function getUsers()
	{
		$this->db->select('id, name');
		$this->db->from('user');
		$this->db->where_not_in('id', 'admin');

		$query = $this->db->get();
		$result = $query->result();
		
		return $result;
	}

	function getAdmin()
	{
		$this->db->select('id');
		$query = $this->db->get_where('user', array('id' => 'admin'));

		$row = $query->row();
		
		return $row;
	}

	function getInfo($id)
	{
		$this->db->select('id, name');
		$query = $this->db->get_where('user', array('id' => $id));
		$row = $query->row();
		
		return $row;
	}

	function modPassword($user)
	{
		$this->db->where('id', $user['id']);
		$this->db->update('user', array('password' => $user['password']));
	}

	function modName($user)
	{
		$this->db->where('id', $user['id']);
		$this->db->update('user', array('name' => $user['name']));
	}
}