<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
class MY_Controller extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
	}

	function _addLog($type, $value, $message, $keyword)
	{
		/* Log Type */
		$logType = array(
			'PAGE_ACCESS',		// 페이지접속
			'USER_LOGIN',		// 로그인
			'USER_LOGOUT',		// 로그아웃
			'STUDENT_SEARCH',	// 학생검색
			'STUDENT_CHECK',	// 학생확인
			'VOTE_COMPLETE',	// 투표등록
			'VOTE_CANCEL'		// 투표취소
		);

		$logPage = array('PAGE_ACCESS');
		$logUser = array('USER_LOGIN', 'USER_LOGOUT');
		$logVote = array('STUDENT_SEARCH', 'STUDENT_CHECK', 'VOTE_COMPLETE', 'VOTE_CANCEL');

		$this->config->load('custom');
		$isLog = $this->config->item('log_enabled');
		
		if($isLog && in_array($type, $logType))
		{
			$id = $this->session->userdata('userID');
			if(empty($id)) $id = '';

			$ip = $this->input->ip_address();

			$log = array(
				'type' => $type,
				'value' => $value,
				'message' => $message,
				'keyword' => $keyword,
				'id' => $id,
				'ip' => $ip
			);

			if(in_array($type, $logPage))
			{
				$this->load->model('log_model');
				$this->log_model->addLogPage($log);
			}

			if(in_array($type, $logUser))
			{
				$this->load->model('log_model');
				$this->log_model->addLogUser($log);
			}

			if(in_array($type, $logVote))
			{
				$this->load->model('log_model');
				$this->log_model->addLogVote($log);
			}
		}
	}

	function _chkLogin($type)
	{
		$isLogin = $this->session->userdata('isLogin');
		$isAdmin = $this->session->userdata('isAdmin');

		if($type == 'admin')
		{
			if(!($isAdmin && $isLogin))
			{
				redirect('/');
			}
		}
		else if($type == 'user')
		{
			if(($isAdmin || !$isLogin))
			{
				redirect('/');
			}
		}
		else
		{
			redirect('/');
		}
	}

	function _head($message = FALSE)
	{
		$isLogin = $this->session->userdata('isLogin');
		$userID = $this->session->userdata('userID');
			
		$this->load->model('user_model');
		$user = $this->user_model->getInfo($userID);

		if(!empty($this->session->flashdata('message')))
		{
			$message = $this->session->flashdata('message');
		}
		
		$this->load->view('head', array(
			'isLogin' => $isLogin,
			'user' => $user,
			'message' => $message
		));
	}

	function _footer()
	{
		$this->load->view('footer');
	}
}