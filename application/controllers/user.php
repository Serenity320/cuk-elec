<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
class User extends MY_Controller
{
	function __construct()
	{
		parent::__construct();

		$this->_chkLogin('admin');
	}

	function password()
	{
		// 투표소, 관리자에 따른 리다이렉션
		$id = $this->input->post('id');
		$id = $this->encrypt->decode($id);

		if($id == 'admin') $redirectURI = '/admin/adminfo';
		else $redirectURI = '/admin/userinfo';

		// 비밀번호 길이 확인
		$this->load->library('form_validation');

		$this->form_validation->set_rules('password', '비밀번호', 'required|min_length[6]|max_length[12]');
		$this->form_validation->set_rules('password2', '비밀번호 확인', 'required|min_length[6]|max_length[12]');

		if($this->form_validation->run() == FALSE)
		{
			$this->session->set_flashdata('message', '비밀번호를 다시 입력해 주세요.(6~12자리)');
			redirect($redirectURI);
		}

		// 비밀번호 확인
		$this->form_validation->set_rules('password', '비밀번호', 'required||matches[password2]');
		$this->form_validation->set_rules('password2', '비밀번호 확인', 'required');

		if($this->form_validation->run() == FALSE)
		{
			$this->session->set_flashdata('message', '비밀번호가 일치하지 않습니다.');
			redirect($redirectURI);
		}

		// 비밀번호 변경
		$password = $this->input->post('password');
		$hash = password_hash($password, PASSWORD_BCRYPT);

		$this->load->model('user_model');
		$this->user_model->modPassword(array(
			'id' => $id,
			'password' => $hash
		));

		$this->session->set_flashdata('message', $id.'의 비밀번호가 변경되었습니다.');
		redirect($redirectURI);
	}

	function name()
	{
		// 투표소 이름 길이 확인
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('name', '이름', 'required||max_length[20]');

		if($this->form_validation->run() == FALSE)
		{
			$this->session->set_flashdata('message', '투표소 이름을 다시 입력해 주세요.(20자리 이하)');
			redirect('/admin/userinfo');
		}

		// 투표소 이름 변경
		$id = $this->input->post('id');
		$name = $this->input->post('name');

		$this->load->model('user_model');
		$this->user_model->modName(array(
			'id'=>$id,
			'name'=>$name
		));

		$this->session->set_flashdata('message', $id.'의  투표소 이름이 변경되었습니다.');
		redirect('/admin/userinfo');
	}
}
?>