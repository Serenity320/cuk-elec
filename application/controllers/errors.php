<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
class Errors extends MY_controller
{
	function __construct()
	{
		parent::__construct();

		/* LOG - PAGE_ACCESS */
		$this->_addLog('PAGE_ACCESS', FALSE, 'PAGE_MISSING', current_url());
	}

	function page_missing()
	{
		$this->_head();
		$this->load->view('error/404');
		$this->_footer();
	}
}
?>