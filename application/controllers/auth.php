<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
class Auth extends MY_Controller
{
	function __construct()
	{
		parent::__construct();
	}

	function login()
	{
		// 로그인 세션 있으면 메인으로 리다이렉션
		if($this->session->userdata('isLogin'))
		{
			redirect('/');
		}

		$this->_head();
		$this->load->view('login');
		$this->_footer();
	}

	function logout()
	{
		/* LOG - USER_LOGOUT */
		$this->_addLog('USER_LOGOUT', TRUE, 'OK_SUCCESS', '');

		$this->session->sess_destroy();

		redirect('/');
	}

	function authentication()
	{
		$id = $this->input->post('id');
		$password = $this->input->post('password');

		if(empty($id) || empty($password))
		{
			/* LOG - USER_LOGIN */
			$this->_addLog('USER_LOGIN', FALSE, 'DO_NO_ENTER_ID_OR_PASSWORD', $id);

			$this->session->set_flashdata('message', '아이디/비밀번호를 입력해 주세요.');
			redirect('/auth/login');
		}

		$this->load->model('user_model');
		$user = $this->user_model->get($id);

		if($id == $user->id && password_verify($password, $user->password))
		{
			$this->session->set_userdata('userID', $user->id);
			$this->session->set_userdata('isLogin', TRUE);

			if($user->id == 'admin')
			{
				$this->session->set_userdata('isAdmin', TRUE);
				$uri = '/admin';
			}
			else
			{
				$uri = '/vote';
			}

			/* LOG - USER_LOGIN */
			$this->_addLog('USER_LOGIN', TRUE, 'OK_SUCCESS', '');

			redirect($uri);
		}
		else
		{
			if(empty($user))
			{
				/* LOG - USER_LOGIN */
				$this->_addLog('USER_LOGIN', FALSE, 'USER_ID_DO_NOT_EXIST', $id);
			}
			else
			{
				/* LOG - USER_LOGIN */
				$this->_addLog('USER_LOGIN', FALSE, 'PASSWORD_DO_NOT_MATCH', $id);
			}
			

			$this->session->set_flashdata('message', '아이디/비밀번호가 일치하지 않습니다.');
			redirect('/auth/login');
		}
	}
}