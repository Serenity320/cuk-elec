<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
class Super extends MY_Controller
{
	function __construct()
	{
		parent::__construct();

		if(base_url() != 'http://127.0.0.1/')
		{
			redirect('/');
		}
	}

	function index()
	{
		$this->_head();
		$this->load->view('admin_menu');

		$this->load->model('user_model');
		$user = $this->user_model->getUsers();
		
		$this->load->model('election_model');
		$election = $this->election_model->get();

		$vote = $this->_getNumOftVotes($user, $election);

		$this->load->view('admin_turnout', array(
			'user' => $user,
			'election' => $election,
			'vote' => $vote
		));
		
		$this->_footer();
	}

	function _getNumOftVotes($user, $election)
	{
		$this->load->model('student_model');
		$this->load->model('vote_model');
		$this->load->model('election_model');

		$election = $this->election_model->get();

		$cntCouncil = array();
		$cntCollege1 = array();
		$cntCollege2 = array();
		$cntCollege3 = array();
		$cntCollege4 = array();
		$club_cnt = array();

		// 총학생회
		$cntCouncil['name'] = '총학생회';
		$cntCouncil['election'] = $election->council;

		$cntCouncil['total'] = $this->student_model->getTotalOfStudnet(); // 총학생회-전체
		$cntCouncil['vote'] = 0;

		foreach($user as $row)
		{
			$cntCouncil['user'][$row->id] = $this->vote_model->getNumOfCouncil($row->id); // 총학생회-투표소
			$cntCouncil['vote'] += $cntCouncil['user'][$row->id]; // 총학생회-투표자
		}

		// 단과대-인예대
		$cntCollege1['name'] = '인예대';
		$cntCollege1['election'] = $election->college1;
		
		$cntCollege1['total'] = $this->student_model->getTotalOfCollege('college1'); // 인예대-전체
		$cntCollege1['vote'] = 0;

		foreach($user as $row)
		{
			$cntCollege1['user'][$row->id] = $this->vote_model->getNumOfCollege('college1', $row->id); // 인예대-투표소
			$cntCollege1['vote'] += $cntCollege1['user'][$row->id]; // 인예대-투표자
		}

		// 단과대-사회대
		$cntCollege2['name'] = '사회대';
		$cntCollege2['election'] = $election->college2;

		$cntCollege2['total'] = $this->student_model->getTotalOfCollege('college2'); // 사회대-전체
		$cntCollege2['vote'] = 0;

		foreach($user as $row)
		{
			$cntCollege2['user'][$row->id] = $this->vote_model->getNumOfCollege('college2', $row->id); // 사회대-투표소
			$cntCollege2['vote'] += $cntCollege2['user'][$row->id]; // 사회대-투표자
		}

		// 단과대-사회대
		$cntCollege3['name'] = '이공대';
		$cntCollege3['election'] = $election->college3;

		$cntCollege3['total'] = $this->student_model->getTotalOfCollege('college3'); // 이공대-전체
		$cntCollege3['vote'] = 0;

		foreach($user as $row)
		{
			$cntCollege3['user'][$row->id] = $this->vote_model->getNumOfCollege('college3', $row->id); // 이공대-투표소
			$cntCollege3['vote'] += $cntCollege3['user'][$row->id]; // 이공대-투표자
		}

		// 단과대-생활대
		$cntCollege4['name'] = '생활대';
		$cntCollege4['election'] = $election->college4;

		$cntCollege4['total'] = $this->student_model->getTotalOfCollege('college4'); // 생활대-전체
		$cntCollege4['vote'] = 0;

		foreach($user as $row)
		{
			$cntCollege4['user'][$row->id] = $this->vote_model->getNumOfCollege('college4', $row->id); // 생활대-투표소
			$cntCollege4['vote'] += $cntCollege4['user'][$row->id]; // 생활대-투표자
		}

		// 총동아리연합회
		$cntClub['name'] = '총동아리연합회';
		$cntClub['election'] = $election->club;

		$cntClub['total'] = $this->student_model->getTotalOfClub(); // 총동아리연합회-전체
		$cntClub['vote'] = 0;

		foreach($user as $row)
		{
			$cntClub['user'][$row->id] = $this->vote_model->getNumOfCulb($row->id); // 총동아리연합회-투표소
			$cntClub['vote'] += $cntClub['user'][$row->id]; // 총동아리연합회-투표자
		}

		$vote = array(
			'cntCouncil' => $cntCouncil,
			'cntCollege1' => $cntCollege1,
			'cntCollege2' => $cntCollege2,
			'cntCollege3' => $cntCollege3,
			'cntCollege4' => $cntCollege4,
			'cntClub' => $cntClub
		);
		
		return $vote;
	}
}
?>