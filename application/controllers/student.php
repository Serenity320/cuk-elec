<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
class Student extends MY_Controller
{
	function __construct()
	{
		parent::__construct();

		$this->_chkLogin('admin');
	}

	function add()
	{
		$this->load->library('form_validation');

		$this->form_validation->set_rules('stuNum', '학번', 'required|exact_length[9]|numeric');
		$this->form_validation->set_rules('stuName', '이름', 'required|max_length[12]');
		$this->form_validation->set_rules('stuMajor', '학부/과', 'required');
		$this->form_validation->set_rules('stuMajor', '제1전공', 'required|max_length[12]');

		if($this->form_validation->run() == FALSE)
		{
			$this->session->set_flashdata('message', '학생의 정보가 올바르지 않습니다.');
			redirect('/admin/register');
		}
		else
		{
			$stuNum = $this->input->post('stuNum');
			$stuName = $this->input->post('stuName');
			$stuMajor = $this->input->post('stuMajor');
			$stuMajor2 = $this->input->post('stuMajor2');
			$stuClub = $this->input->post('stuClub');

			$this->load->model('student_model');
			$chkStu = $this->student_model->get($stuNum);

			if($chkStu)
			{
				$this->session->set_flashdata('message', '동일한 학번의 학생이 있습니다.');
				redirect('/admin/register');
			}
			else
			{
				if(!empty($stuClub)) $stuClub = TRUE;
				else $stuClub = FALSE;

				$student = $this->student_model->add(array(
					'stuNum' => $stuNum,
					'stuName' => $stuName,
					'stuMajor' => $stuMajor,
					'stuMajor2' => $stuMajor2,
					'stuClub' => $stuClub
				));

				$this->session->set_flashdata('stuNum', $stuNum);
				$this->session->set_flashdata('message', '학생이 추가되었습니다.');

				redirect('/admin/search');
			}
		}
	}

	function club()
	{
		$stuNum = $this->input->post('stuNum');
		$stuNum = $this->encrypt->decode($stuNum);

		$this->load->model('vote_model');
		$chkVote = $this->vote_model->count($stuNum);

		if($chkVote > 0)
		{
			$this->session->set_flashdata('stuNum', $stuNum);
			$this->session->set_flashdata('message', '이미 투표 한 학생은 수정할 수 없습니다.');
		}
		else
		{
			$this->load->model('student_model');
			$student = $this->student_model->get($stuNum);

			if(empty($student))
			{
				$this->session->set_flashdata('message', '학생이 존재하지 않습니다.');
				redirect('/admin/search');
			}

			$this->load->model('student_model');

			$student = $this->student_model->get($stuNum);
			$this->student_model->modifyClub($stuNum, !$student->club);

			$this->session->set_flashdata('stuNum', $stuNum);
			$this->session->set_flashdata('message', '동아리 가입 여부가 수정되었습니다.');
		}

		redirect('/admin/search');
	}
}