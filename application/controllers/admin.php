<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
class Admin extends MY_Controller
{
	function __construct()
	{
		parent::__construct();

		$this->_chkLogin('admin');
	}

	function index()
	{
		redirect('/admin/search');
	}

	function search()
	{
		$this->_head();
		$this->load->view('admin_menu');

		$stuNum = $this->session->flashdata('stuNum');

		if(empty($stuNum))
		{
			$stuNum = $this->input->post('stuNum');
		}

		$this->load->model('student_model');
		$student = $this->student_model->get($stuNum);

		$isImage = FALSE;

		if(!empty($student))
		{
			$this->load->helper('file');
			$isImage = read_file('./static/img/student/'.$student->num.'.jpg');
		}

		$this->load->view('admin_search', array(
			'student' => $student,
			'isImage' => $isImage
		));
		
		$this->_footer();
	}

	function register()
	{
		$this->_head();
		$this->load->view('admin_menu');

		$this->load->model('student_model');
		$major = $this->student_model->getMajors();

		$this->load->view('admin_register', array('major' => $major));
		
		$this->_footer();
	}

	function voter()
	{
		$this->_head();
		$this->load->view('admin_menu');

		$this->load->model('vote_model');
		$vote = $this->vote_model->get();

		$this->load->view('admin_voter', array('vote'=>$vote));
		
		$this->_footer();
	}

	function turnout()
	{
		$this->_head();
		$this->load->view('admin_menu');

		$this->load->model('user_model');
		$user = $this->user_model->getUsers();
		
		$this->load->model('election_model');
		$election = $this->election_model->get();

		$vote = $this->_getNumOftVotes($user, $election);

		$this->load->view('admin_turnout', array(
			'user' => $user,
			'election' => $election,
			'vote' => $vote
		));
		
		$this->_footer();
	}

	function election()
	{
		$this->_head();
		$this->load->view('admin_menu');

		$this->load->model('election_model');
		$election = $this->election_model->get();

		$this->load->view('admin_election', array('election' => $election));

		$this->_footer();
	}

	function electmod()
	{
		$council = $this->input->post('chkCouncil');
		$college1 = $this->input->post('chkCollege1');
		$college2 = $this->input->post('chkCollege2');
		$college3 = $this->input->post('chkCollege3');
		$college4 = $this->input->post('chkCollege4');
		$club = $this->input->post('chkClub');

		$password = $this->input->post('password');
		
		if(empty($council) && (empty($college1) && empty($college2) && empty($college3) && empty($college4)) && empty($club))
		{
			$this->session->set_flashdata('message', '한 개 이상은 투표를 설정해야 합니다.');
			redirect('/admin/election');
		}

		$this->load->model('user_model');
		$user = $this->user_model->get('admin');

		if(empty($password))
		{
			$this->session->set_flashdata('message', '비밀번호를 입력하시기 바랍니다.');
		}
		else if(!password_verify($password, $user->password))
		{
			$this->session->set_flashdata('message', '비밀번호가 일치하지 않습니다.');
		}
		else
		{
			if(!empty($council)) $council = TRUE;
			if(!empty($college1)) $college1 = TRUE;
			if(!empty($college2)) $college2 = TRUE;
			if(!empty($college3)) $college3 = TRUE;
			if(!empty($college4)) $college4 = TRUE;
			if(!empty($club)) $club = TRUE;

			$this->load->model('election_model');
			$this->election_model->modify(array(
				'council' => $council,
				'college1' => $college1,
				'college2' => $college2,
				'college3' => $college3,
				'college4' => $college4,
				'club' => $club,
			));

			$this->session->set_flashdata('message', '투표 정보가 초기화되었습니다.');
		}
		
		redirect('/admin/election');
	}

	function userinfo()
	{
		$this->_head();
		$this->load->view('admin_menu');

		$this->load->model('user_model');
		$user = $this->user_model->getUsers();

		$this->load->view('admin_userinfo', array(
			'user' => $user,
			'cnt' => 1
		));
		
		$this->_footer();
	}

	function adminfo()
	{
		$this->_head();
		$this->load->view('admin_menu');

		$this->load->model('user_model');
		$admin = $this->user_model->getAdmin();

		$this->load->view('admin_adminfo', array('admin' => $admin));
		
		$this->_footer();
	}

	function _getNumOftVotes($user, $election)
	{
		$this->load->model('student_model');
		$this->load->model('vote_model');
		$this->load->model('election_model');

		$election = $this->election_model->get();

		$cntCouncil = array();
		$cntCollege1 = array();
		$cntCollege2 = array();
		$cntCollege3 = array();
		$cntCollege4 = array();
		$club_cnt = array();

		// 총학생회
		$cntCouncil['name'] = '총학생회';
		$cntCouncil['election'] = $election->council;

		$cntCouncil['total'] = $this->student_model->getTotalOfStudnet(); // 총학생회-전체
		$cntCouncil['vote'] = 0;

		foreach($user as $row)
		{
			$cntCouncil['user'][$row->id] = $this->vote_model->getNumOfCouncil($row->id); // 총학생회-투표소
			$cntCouncil['vote'] += $cntCouncil['user'][$row->id]; // 총학생회-투표자
		}

		// 단과대-인예대
		$cntCollege1['name'] = '인예대';
		$cntCollege1['election'] = $election->college1;
		
		$cntCollege1['total'] = $this->student_model->getTotalOfCollege('college1'); // 인예대-전체
		$cntCollege1['vote'] = 0;

		foreach($user as $row)
		{
			$cntCollege1['user'][$row->id] = $this->vote_model->getNumOfCollege('college1', $row->id); // 인예대-투표소
			$cntCollege1['vote'] += $cntCollege1['user'][$row->id]; // 인예대-투표자
		}

		// 단과대-사회대
		$cntCollege2['name'] = '사회대';
		$cntCollege2['election'] = $election->college2;

		$cntCollege2['total'] = $this->student_model->getTotalOfCollege('college2'); // 사회대-전체
		$cntCollege2['vote'] = 0;

		foreach($user as $row)
		{
			$cntCollege2['user'][$row->id] = $this->vote_model->getNumOfCollege('college2', $row->id); // 사회대-투표소
			$cntCollege2['vote'] += $cntCollege2['user'][$row->id]; // 사회대-투표자
		}

		// 단과대-사회대
		$cntCollege3['name'] = '이공대';
		$cntCollege3['election'] = $election->college3;

		$cntCollege3['total'] = $this->student_model->getTotalOfCollege('college3'); // 이공대-전체
		$cntCollege3['vote'] = 0;

		foreach($user as $row)
		{
			$cntCollege3['user'][$row->id] = $this->vote_model->getNumOfCollege('college3', $row->id); // 이공대-투표소
			$cntCollege3['vote'] += $cntCollege3['user'][$row->id]; // 이공대-투표자
		}

		// 단과대-생활대
		$cntCollege4['name'] = '생활대';
		$cntCollege4['election'] = $election->college4;

		$cntCollege4['total'] = $this->student_model->getTotalOfCollege('college4'); // 생활대-전체
		$cntCollege4['vote'] = 0;

		foreach($user as $row)
		{
			$cntCollege4['user'][$row->id] = $this->vote_model->getNumOfCollege('college4', $row->id); // 생활대-투표소
			$cntCollege4['vote'] += $cntCollege4['user'][$row->id]; // 생활대-투표자
		}

		// 총동아리연합회
		$cntClub['name'] = '총동아리연합회';
		$cntClub['election'] = $election->club;

		$cntClub['total'] = $this->student_model->getTotalOfClub(); // 총동아리연합회-전체
		$cntClub['vote'] = 0;

		foreach($user as $row)
		{
			$cntClub['user'][$row->id] = $this->vote_model->getNumOfCulb($row->id); // 총동아리연합회-투표소
			$cntClub['vote'] += $cntClub['user'][$row->id]; // 총동아리연합회-투표자
		}

		$vote = array(
			'cntCouncil' => $cntCouncil,
			'cntCollege1' => $cntCollege1,
			'cntCollege2' => $cntCollege2,
			'cntCollege3' => $cntCollege3,
			'cntCollege4' => $cntCollege4,
			'cntClub' => $cntClub
		);
		
		return $vote;
	}
}
?>