<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
class Vote extends MY_Controller
{
	function __construct()
	{
		parent::__construct();

		$this->_chkLogin('user');
	}

	function index()
	{
		$chkVote = $this->session->userdata('chkVote');

		if(!empty($chkVote))
		{
			$this->session->unset_userdata('chkVote');

			/* LOG - STUDENT_SEARCH */
			$this->_addLog('STUDENT_SEARCH', FALSE, 'DO_NOT_VOTE_AFTER_SEARCH', $chkVote);

			$message = '학번검색 후 투표를 하지 않았습니다!';
			$this->_head($message);
		}
		else
		{
			$this->_head();
		}		

		$this->load->view('vote_main');

		$stuNum = $this->session->flashdata('stuNum');
		$fromVail = TRUE;

		if(empty($stuNum))
		{
			$stuNum = $this->input->post('stuNum');

			$this->load->library('form_validation');
			$this->form_validation->set_rules('stuNum', '학번', 'required|exact_length[9]|numeric');
			$fromVail = $this->form_validation->run();
		}

		if($fromVail == FALSE)
		{
			if(empty($stuNum))
			{
				$message = '학번을 검색해 주세요.';
				$this->load->view('vote_message', array('message' => $message));
			}
			else
			{
				/* LOG - STUDENT_SEARCH */
				$this->_addLog('STUDENT_SEARCH', FALSE, 'INCORRECT_STUDENT_NUMBER', $stuNum);

				$message = '학번을 다시 입력해 주세요. (9자리 숫자)';
				$this->load->view('vote_message', array('message' => $message));
			}
		}
		else
		{
			$this->load->model('student_model');
			$student = $this->student_model->get($stuNum);
			
			if(empty($student))
			{
				/* LOG - STUDENT_SEARCH */
				$this->_addLog('STUDENT_SEARCH', FALSE, 'STUDENT_DO_NOT_EXIST', $stuNum);

				$message = '학생이 존재하지 않습니다.';
				$this->load->view('vote_message', array('message' => $message));
			}
			else
			{
				$this->load->helper('file');
				$isImage = read_file('./static/img/student/'.$student->num.'.jpg');

				if(!empty($isImage)) $isImage = TRUE;

				$this->load->view('vote_result', array(
					'student' => $student,
					'isImage' => $isImage
				));

				$this->load->model('vote_model');
				$cntVote = $this->vote_model->count($stuNum);

				if($cntVote == 0)
				{
					// 투표가능 로그 추가

					$this->load->model('election_model');

					$collage = $this->student_model->getCollage($stuNum);
					$election = $this->election_model->get();

					$this->load->view('vote_select', array(
						'stuNum' => $stuNum,
						'collage' => $collage,
						'isClub' => $student->club,
						'election' => $election
					));

					// 검색 후 투표확인 세션
					$this->session->set_userdata('chkVote', $stuNum);
				}
				else
				{
					// 투표했음 로그 추가

					$this->load->view('vote_complete', array('stuNum' => $stuNum));
				}


				if(empty($this->session->flashdata('stuNum')))
				{
					/* LOG - STUDENT_SEARCH */
					$this->_addLog('STUDENT_SEARCH', TRUE, 'OK_SUCCESS', $stuNum);
				}
				else
				{
					/* LOG - STUDENT_CHECK */
					$this->_addLog('STUDENT_CHECK', TRUE, 'OK_SUCCESS', $stuNum);
				}
			}
		}

		$this->_footer();
	}

	function complete()
	{
		// 검색 후 투표확인 세션 - 해제
		$this->session->unset_userdata('chkVote');

		$stuNum = $this->input->post('stuNum');
		$stuNum = $this->encrypt->decode($stuNum);

		$council = $this->input->post('chkCouncil');
		$college1 = $this->input->post('chkCollege1');
		$college2 = $this->input->post('chkCollege2');
		$college3 = $this->input->post('chkCollege3');
		$college4 = $this->input->post('chkCollege4');
		$club = $this->input->post('chkClub');

		$this->load->model('student_model');
		$student = $this->student_model->get($stuNum);

		if(empty($student))
		{
			/* LOG - VOTE_COMPLETE */
			$this->_addLog('VOTE_COMPLETE', FALSE, 'STUDENT_DO_NOT_EXIST', $stuNum);

			$this->session->set_flashdata('message', '학생이 존재하지 않습니다.');
			redirect('/vote');
		}

		if(empty($council) && (empty($college1) && empty($college2) && empty($college3) && empty($college4)) && empty($club))
		{
			/* LOG - VOTE_COMPLETE */
			$this->_addLog('VOTE_COMPLETE', FALSE, 'AT_LEAST_ONE_MUST_VOTE', $stuNum);

			$this->session->set_flashdata('stuNum', $stuNum);
			$this->session->set_flashdata('message', '한 개 이상은 투표를 헤야 합니다.');

			redirect('/vote');
		}
		
		$this->load->model('vote_model');
		$cntVote = $this->vote_model->count($stuNum);

		if($cntVote > 0)
		{
			$this->session->set_flashdata('message', '이미 투표를 한 학생입니다.');

			/* LOG - VOTE_COMPLETE */
			$this->_addLog('VOTE_COMPLETE', FALSE, 'HAVE_ALREADY_VOTED', $stuNum);
		}
		else
		{
			$userID = $this->session->userdata('userID');

			$vote = array(
				'stuNum' => $stuNum,
				'council' => $council,
				'college1' => $college1,
				'college2' => $college2,
				'college3' => $college3,
				'college4' => $college4,
				'club' => $club
			);

			if(!empty($vote['council'])) $vote['council'] = $userID;
			else $vote['council'] = '';

			if(!empty($vote['college1'])) $vote['college1'] = $userID;
			else $vote['college1'] = '';

			if(!empty($vote['college2'])) $vote['college2'] = $userID;
			else $vote['college2'] = '';

			if(!empty($vote['college3'])) $vote['college3'] = $userID;
			else $vote['college3'] = '';

			if(!empty($vote['college4'])) $vote['college4'] = $userID;
			else $vote['college4'] = '';

			if(!empty($vote['club'])) $vote['club'] = $userID;
			else $vote['club'] = '';

			$this->vote_model->add($vote);

			/* LOG - VOTE_COMPLETE */
			$this->_addLog('VOTE_COMPLETE', TRUE, 'OK_SUCCESS', $stuNum);
		}

		$this->session->set_flashdata('stuNum', $stuNum);
		redirect('/vote');
	}

	function cancel()
	{
		$stuNum = $this->input->post('stuNum');
		$stuNum = $this->encrypt->decode($stuNum);

		$this->load->model('student_model');
		$student = $this->student_model->get($stuNum);

		if(empty($student))
		{
			/* LOG - VOTE_CANCEL */
			$this->_addLog('VOTE_CANCEL', FALSE, 'STUDENT_DO_NOT_EXIST', $stuNum);

			$this->session->set_flashdata('message', '학생이 존재하지 않습니다.');
			redirect('/vote');
		}

		$this->load->model('vote_model');
		$cntVote = $this->vote_model->count($stuNum);

		if($cntVote == 0)
		{
			/* LOG - VOTE_CANCEL */
			$this->_addLog('VOTE_CANCEL', FALSE, 'STUDENT_DO_NOT_VOTE', $stuNum);

			$this->session->set_flashdata('message', '투표를 해야 취소가 가능합니다.');
			redirect('/vote');
		}
		else
		{
			$this->load->model('vote_model');
			$this->vote_model->delete($stuNum);

			/* LOG - VOTE_CANCEL */
			$this->_addLog('VOTE_CANCEL', TRUE, 'OK_SUCCESS', $stuNum);

			$this->session->set_flashdata('stuNum', $stuNum);
			redirect('/vote');
		}
	}
}
?>