<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
class Main extends MY_Controller
{
	function __construct()
	{
		parent::__construct();
	}

	function index()
	{
		if($this->session->userdata('isLogin'))
		{
			if($this->session->userdata('isAdmin'))
			{
				/* LOG - PAGE_ACCESS */
				$this->_addLog('PAGE_ACCESS', TRUE, 'FROM_INDEX_TO_ADMIN', current_url());

				redirect('/admin');
			}
			else
			{
				/* LOG - PAGE_ACCESS */
				$this->_addLog('PAGE_ACCESS', TRUE, 'FROM_INDEX_TO_VOTE', current_url());

				redirect('/vote');
			}
		}
		else
		{
			/* LOG - PAGE_ACCESS */
			$this->_addLog('PAGE_ACCESS', TRUE, 'FROM_INDEX_TO_LOGIN', current_url());

			redirect('/auth/login');
		}
	}
}
?>