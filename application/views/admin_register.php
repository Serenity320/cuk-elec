			<div class="col-lg-10">
				<h1 class="page-header">학생 등록</h1>
				<div class="panel panel-default">
					<div class="panel-body">
						<form class="form-horizontal" role="form" action="/student/add" method="post">
							<div class="form-group">
								<label class="col-lg-2 control-label">학번</label>
								<div class="col-lg-3">
									<input type="text" name="stuNum" class="form-control" placeholder="학번" maxlength="9" />
								</div>
								<div class="col-lg-7"></div>
							</div>
							<div class="form-group">
								<label class="col-lg-2 control-label">이름</label>
								<div class="col-lg-3">
									<input type="text" name="stuName" class="form-control" placeholder="이름" maxlength="12" />
								</div>
								<div class="col-lg-7"></div>
							</div>
							<div class="form-group">
								<label class="col-lg-2 control-label">학부/과</label>
								<div class="col-lg-3">
									<select name="stuMajor" class="form-control">
										<?php foreach($major as $row): ?>
										<option value="<?=$row->major?>"><?=$row->major?>
										<?php endforeach; ?>
									</select>
								</div>
								<div class="col-lg-7"></div>
							</div>
							<div class="form-group">
								<label class="col-lg-2 control-label">제1전공</label>
								<div class="col-lg-3">
									<input type="text" name="stuMajor2" class="form-control" placeholder="제1전공" maxlength="12" />
								</div>
								<div class="col-lg-7"></div>
							</div>
							<div class="form-group">
								<label class="col-lg-2 control-label">동아리 가입 여부</label>
								<div class="col-lg-3">
									<label class="checkbox-inline">
										<input type="checkbox" name="stuClub" />　
									</label>
								</div>
								<div class="col-lg-7"></div>
							</div>
							<div align="center">
								<button type="submit" class="btn btn-default">학생 등록</button>
							</div>
						</form>
					</div>
				</div>
			</div>	
		</div>
