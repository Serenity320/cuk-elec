			<div class="col-lg-10">
				<h1 class="page-header">관리자 정보</h1>
				<div class="panel panel-default">
					<div class="panel-body">
						<form class="form-horizontal" role="form" action="/user/password" method="post">
							<div class="form-group">
								<label class="col-lg-2 control-label">아이디</label>
								<div class="col-lg-3">
									<p class="form-control-static"><?=$admin->id?></p>
								</div>
								<div class="col-lg-7"></div>
							</div>
							<div class="form-group">
								<label class="col-lg-2 control-label">비밀번호</label>
								<div class="col-lg-3">
									<input type="password" name="password" class="form-control" placeholder="비밀번호" maxlength="12" />
								</div>
								<div class="col-lg-7"></div>
							</div>
							<div class="form-group">
								<label class="col-lg-2 control-label">비밀번호 확인</label>
								<div class="col-lg-3">
									<input type="password" name="password2" class="form-control" placeholder="비밀번호 확인" maxlength="12" />
								</div>
								<div class="col-lg-7">
									<input type="hidden" name="id" value="<?=$this->encrypt->encode($admin->id)?>" />
									<button type="submit" class="btn btn-default">비밀번호 변경</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>	
		</div>
