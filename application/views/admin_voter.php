			<div class="col-lg-10">
				<h1 class="page-header">투표자 목록</h1>
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="table-responsive">
							<table class="table table-striped">
								<thead>
									<tr>
										<th>학번</th>
										<th>이름</th>
										<th>학부/과</th>
										<th>제1전공</th>
										<th>동아리</th>
										<th>투표시간</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach($vote as $row): ?>
									<tr>
										<td><?=$row->num?></td>
										<td><?=$row->name?></td>
										<td><?=$row->major?></td>
										<td><?=$row->major2?></td>
										<td>
											<?php if($row->club): ?>O
											<?php else: ?>X
											<?php endif; ?>
										</td>
										<td><?=$row->created?></td>
									</tr>
									<?php endforeach; ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
