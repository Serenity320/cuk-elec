		<div class="panel panel-default">
			<div class="panel-body">
				<div class="table-responsive">
					<table class="table table-bordered">
						<tr height="20px">
							<td rowspan="5" width="150px" align="center">
								<?php if($isImage): ?>
								<img src="/static/img/student/<?=$student->num?>.jpg" class="img-rounded" width="150" height="200">
								<?php else: ?>
								<img src="/static/img/noimage.gif" class="img-rounded" width="150" height="200">
								<?php endif; ?>
							</td>
							<th width="200px" height="20%">학번</th>
							<td><?=$student->num?></td>
						</tr>
						<tr>
							<th width="200px" height="20%">이름</th>
							<td><?=$student->name?></td>
						</tr>
						<tr>
							<th width="200px" height="20%">학부/과</th>
							<td><?=$student->major?></td>
						</tr>
						<tr>
							<th width="200px" height="20%">제1전공</th>
							<td><?=$student->major2?></td>
						</tr>
						<tr>
							<th width="200px" height="20%">동아리 가입 여부</th>
							<td>
								<?php if($student->club): ?>O
								<?php else: ?>X
								<?php endif; ?>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
