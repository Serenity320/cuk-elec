<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>가톨릭대학교 중앙선거관리위원회</title>

	<!-- Bootstrap -->
	<link href="/static/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

	<style>
		body { padding-top: 70px; }
		.form_login { padding-top: 50px; }
		.form_submit { padding-top: 15px; }
	</style>

	<?php if($message): ?>
	<script>
		alert('<?=$message?>')
	</script>
	<?php endif; ?>
	
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body>
	<nav class="navbar navbar-default navbar-fixed-top">
		<div class="container-fluid">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#"><small>가톨릭대학교 중앙선거관리위원회</small></a>
			</div>
			
			<!-- Collect the nav links, forms, and other content for toggling -->
			<?php if($isLogin): ?>
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav navbar-right">
					<li><a href="#"><strong><?=$user->id?>(<?=$user->name?>)</strong></a></li>
					<li><a href="/auth/logout">[로그아웃]</a></li>
				</ul>
			</div>
			<?php endif; ?>
		</div>
	</nav>
	<div class="container bs-docs-container">
