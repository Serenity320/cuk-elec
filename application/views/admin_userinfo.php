			<div class="col-lg-10">
				<h1 class="page-header">투표소 정보</h1>
				<?php foreach($user as $row): ?>
				<div class="panel panel-default">
					<div class="panel-body">
						<form class="form-horizontal" role="form" action="/user/password" method="post">
							<div class="form-group">
								<label class="col-lg-2 control-label">아이디</label>
								<div class="col-lg-3">
									<p class="form-control-static"><?=$row->id?> <strong>[투표소<?=$cnt++?>]</strong></p>
								</div>
								<div class="col-lg-7"></div>
							</div>
							<div class="form-group">
								<label class="col-lg-2 control-label">비밀번호</label>
								<div class="col-lg-3">
									<input type="password" name="password" class="form-control" placeholder="비밀번호" maxlength="12" />
								</div>
								<div class="col-lg-7"></div>
							</div>
							<div class="form-group">
								<label class="col-lg-2 control-label">비밀번호 확인</label>
								<div class="col-lg-3">
									<input type="password" name="password2" class="form-control" placeholder="비밀번호 확인" maxlength="12" />
								</div>
								<div class="col-lg-7">
									<input type="hidden" name="id" value="<?=$this->encrypt->encode($row->id)?>" />
									<button type="submit" class="btn btn-default">비밀번호 변경</button>
								</div>
							</div>
						</form>
						<form class="form-horizontal" role="form" action="/user/name" method="post">
							<div class="form-group">
								<label class="col-lg-2 control-label">투표소 이름</label>
								<input type="hidden" name="id" value="<?=$row->id?>" />
								<div class="col-lg-5">
									<input type="text" name="name" class="form-control" value="<?=$row->name?>" placeholder="투표소 이름" maxlength="20" />
								</div>
								<div class="col-lg-5">
									<button type="submit" class="btn btn-default">이름 변경</button>
								</div>
							</div>
						</form>
					</div>
				</div>
				<?php endforeach; ?>
			</div>	
		</div>
