			<div class="col-lg-10">
				<h1 class="page-header">학생 검색</h1>
				<div class="well" align="center">
					<form class="form-inline" role="form" action="/admin/search" method="post">
						<div class="form-group">
							<input type="text" name="stuNum" class="form-control" placeholder="학번" maxlength="9" autofocus />
						</div>
						<button type="submit" class="btn btn-default">검색</button>
					</form>
				</div>
				<?php if(!empty($student)): ?>
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="table-responsive">
							<table class="table table-bordered">
								<tr height="20px">
									<td rowspan="6" width="150px" align="center">
										<?php if($isImage): ?>
										<img src="/static/img/student/<?=$student->num?>.jpg" class="img-rounded" width="150" height="200">
										<?php else: ?>
										<img src="/static/img/noimage.gif" class="img-rounded" width="150" height="200">
										<?php endif; ?>
									</td>
									<th width="200px" height="20%">학번</th>
									<td><?=$student->num?></td>
								</tr>
								<tr>
									<th width="200px" height="20%">이름</th>
									<td><?=$student->name?></td>
								</tr>
								<tr>
									<th width="200px" height="20%">학부/과</th>
									<td><?=$student->major?></td>
								</tr>
								<tr>
									<th width="200px" height="20%">제1전공</th>
									<td><?=$student->major2?></td>
								</tr>
								<tr>
									<th width="200px" height="20%">동아리 가입 여부</th>
									<td>
										<form class="form-inline" role="form" action="/student/club" method="post">
											<div class="form-group">
												<?php if($student->club): ?>O
												<?php else: ?>X
												<?php endif; ?>
												&nbsp;
											</div>
											<input type="hidden" name="stuNum" value="<?=$this->encrypt->encode($student->num)?>" />
											<button type="submit" class="btn btn-default btn-sm">수정</button>
										</form>
									</td>
								</tr>
							</table>
						</div>
					</div>
				</div>
				<?php endif; ?>
			</div>
		</div>
