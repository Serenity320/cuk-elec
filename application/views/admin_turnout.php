			<div class="col-lg-10">
				<h1 class="page-header">투표율 정보</h1>
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="table-responsive">
							<table class="table table-striped">
								<thead>
									<tr>
										<th>종류</th>
										<th>투표소1</th>
										<th>투표소2</th>
										<th>투표소3</th>
										<th>투표소4</th>
										<th>투표소5</th>
										<th>합계</th>
									</tr>
								</thead>
								<tbody>
								<?php foreach($vote as $row1): ?>
									<tr>
										<th rowspan="2"><?=$row1['name']?></th>
										<?php foreach($user as $row2): ?>
										<?php if($row1['election']): ?>
										<td><?=number_format($row1['user'][$row2->id])?>명</td>
										<?php else: ?>
										<td>-</td>
										<?php endif; ?>
										<?php endforeach; ?>
										<?php if($row1['election']): ?>
										<th><?=number_format($row1['vote'])?>명 / <?=number_format($row1['total'])?>명</th>
										<?php else: ?>
										<td>-</td>
										<?php endif; ?>
									</tr>
									<tr>
										<?php foreach($user as $row2): ?>
										<?php if($row1['election']): ?>
										<th><?=@number_format(($row1['user'][$row2->id] / $row1['total']) * 100, 2)?>%</th>
										<?php else: ?>
										<td>-</td>
										<?php endif; ?>
										<?php endforeach; ?>
										<?php if($row1['election']): ?>
										<th><?=@number_format(($row1['vote'] / $row1['total']) * 100, 2)?>%</th>
										<?php else: ?>
										<td>-</td>
										<?php endif; ?>
									</tr>
								<?php endforeach; ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				</div>
			</div>
		</div>
