		<div class="row">
			<div class="col-lg-2">
				<ul class="nav nav-pills nav-stacked">
					<li><a href="/admin/search">학생 검색</a></li>
					<li><a href="/admin/register">학생 등록</a></li>
					<li><a href="/admin/voter">투표자 목록</a></li>
					<li><a href="/admin/turnout">투표율 정보</a></li>
					<li><a href="/admin/election">선거 관리</a></li>
					<li><a href="/admin/userinfo">투표소 정보</a></li>
					<li><a href="/admin/adminfo">관리자 정보</a></li>
				</ul>
			</div>
