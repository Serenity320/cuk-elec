		<form class="form-inline" role="form" action="/vote/complete" method="post">
			<div class="panel panel-default" align="center">
				<div class="panel-body">
					<div class="row">
						<div class="col-lg-2"></div>
						<div class="col-lg-2">
							<label class="checkbox-inline">
								<?php if($election->council): ?>
								<input type="checkbox" name="chkCouncil" checked /> <strong>총학생회</strong>
								<?php else: ?>
								<input type="checkbox" name="chkCouncil" disabled /> <strike>총학생회</strike>
								<?php endif; ?>
							</label>
						</div>
						<div class="col-lg-4">
							<label class="checkbox-inline">
								<?php if($election->college1 && $collage->type == 'college1'): ?>
								<input type="checkbox" name="chkCollege1" checked /> <strong>인예대</strong>
								<?php else: ?>
								<input type="checkbox" name="chkCollege1" disabled /> <strike>인예대</strike>
								<?php endif; ?>
							</label>
							<label class="checkbox-inline">
								<?php if($election->college2 && $collage->type == 'college2'): ?>
								<input type="checkbox" name="chkCollege2" checked /> <strong>사회대</strong>
								<?php else: ?>
								<input type="checkbox" name="chkCollege2" disabled /> <strike>사회대</strike>
								<?php endif; ?>
							</label>
							<label class="checkbox-inline">
								<?php if($election->college3 && $collage->type == 'college3'): ?>
								<input type="checkbox" name="chkCollege3" checked /> <strong>이공대</strong>
								<?php else: ?>
								<input type="checkbox" name="chkCollege3" disabled /> <strike>이공대</strike>
								<?php endif; ?>
							</label>
							<label class="checkbox-inline">
								<?php if($election->college4 && $collage->type == 'college4'): ?>
								<input type="checkbox" name="chkCollege4" checked /> <strong>생활대</strong>
								<?php else: ?>
								<input type="checkbox" name="chkCollege4" disabled /> <strike>생활대</strike>
								<?php endif; ?>
							</label>
						</div>
						<div class="col-lg-2">
							<label class="checkbox-inline">
								<?php if($election->club && $isClub): ?>
								<input type="checkbox" name="chkClub" checked /> <strong>총동아리연합회</strong>
								<?php else: ?>
								<input type="checkbox" name="chkClub" disabled /> <strike>총동아리연합회</strike>
								<?php endif; ?>
							</label>
						</div>
						<div class="col-lg-2"></div>
					</div>
				</div>
			</div>
			<div align="center">
				<input type="hidden" name="stuNum" value="<?=$this->encrypt->encode($stuNum)?>" />
				<button type="submit" class="btn btn-default">투표 하기</button>
			</div>
		</form>
