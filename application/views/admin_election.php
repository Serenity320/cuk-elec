			<div class="col-lg-10">
				<h1 class="page-header">선거 관리</h1>
				<div class="panel panel-default" align="center">
					<div class="panel-body">
						<form class="form-inline" role="form" action="/admin/electmod" method="post">
							<div class="row">
								<div class="col-lg-1"></div>
								<div class="col-lg-2">
									<label class="checkbox-inline">
										<?php if($election->council): ?>
										<input type="checkbox" name="chkCouncil" checked /> 총학생회
										<?php else: ?>
										<input type="checkbox" name="chkCouncil" /> 총학생회
										<?php endif; ?>
									</label>
								</div>
								<div class="col-lg-6">
									<label class="checkbox-inline">
										<?php if($election->college1): ?>
										<input type="checkbox" name="chkCollege1" checked /> 인예대
										<?php else: ?>
										<input type="checkbox" name="chkCollege1" /> 인예대
										<?php endif; ?>
									</label>
									<label class="checkbox-inline">
										<?php if($election->college2): ?>
										<input type="checkbox" name="chkCollege2" checked /> 사회대
										<?php else: ?>
										<input type="checkbox" name="chkCollege2" /> 사회대
										<?php endif; ?>
									</label>
									<label class="checkbox-inline">
										<?php if($election->college3): ?>
										<input type="checkbox" name="chkCollege3" checked /> 이공대
										<?php else: ?>
										<input type="checkbox" name="chkCollege3" /> 이공대
										<?php endif; ?>
									</label>
									<label class="checkbox-inline">
										<?php if($election->college4): ?>
										<input type="checkbox" name="chkCollege4" checked /> 생활대
										<?php else: ?>
										<input type="checkbox" name="chkCollege4" /> 생활대
										<?php endif; ?>
									</label>
								</div>
								<div class="col-lg-2">
									<label class="checkbox-inline">
										<?php if($election->club): ?>
										<input type="checkbox" name="chkClub" checked /> 총동아리연합회
										<?php else: ?>
										<input type="checkbox" name="chkClub" /> 총동아리연합회
										<?php endif; ?>
									</label>
								</div>
								<div class="col-lg-1"></div>
							</div>
							<div class="form_submit">
								<input type="password" name="password" class="form-control" placeholder="비밀번호" />
								<button type="submit" class="btn btn-default">선거 초기화</button>
							</div>
						</form>
					</div>
				</div>
				<div class="alert alert-warning" align="center">투표를 수정하면 기존 투표 정보가 초기화 됩니다.</div>
			</div>
		</div>
